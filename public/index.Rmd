---
pagetitle: "Lectura 6"
title: "\nTaller de R: Estadística y programación\n"
subtitle: "\nLectura 6: Dirty data y Cleaning data (parte 3) \n"
author: "\nEduard F. Martínez-González\n"
date: "Universidad de los Andes | [ECON-1302](https://github.com/taller-R)"
output: 
  revealjs::revealjs_presentation:  
    theme: simple 
    highlight: tango
    center: true
    nature:
      transition: slide
      self_contained: false # para que funcione sin internet
      highlightStyle: github
      highlightLines: true
      countIncrementalSlides: true
      showSlideNumber: 'all'
    seal: true # remover la primera diapositiva (https://github.com/yihui/xaringan/issues/84)
    # Help 1: https://revealjs.com/presentation-size/  
    # Help 2: https://bookdown.org/yihui/rmarkdown/revealjs.html
    # Estilos revealjs: https://github.com/hakimel/reveal.js/blob/master/css/theme/template/theme.scss
---

```{r setup, include=F , cache=F}
# load packages
library(pacman)
pacman::p_load(rio, tidyverse, skimr, DT, here, knitr, ggthemes)
opts_chunk$set(fig.align="center", fig.height=4 , dpi=300 , cache=F) # chunk options

ocu_1_2 = import("data/ocupados mes 1 & 2.RDS")
```

## Hoy veremos

### **[1.]** group_by, mutate & summarise

  * 1.1 Summarize
  
  * 1.2 Summarize + group_by
  
  * 1.3 Summarize + group_by + mutate

### **[2.]** pivotear
        
  * 2.1 Pivot wide
   
  * 2.2 Pivot long
  
<!------------------------------>

## Inspeccion de datos

- Utilizaremos los datos creados en la clase anterior. 
```{r, echo=FALSE, message=FALSE}
ocu_1_2 %>% select(directorio, secuencia_p, orden, mes, p6020, p6040, p6160, p6500, p6426) %>%  head()
```
$$\\[0.2in]$$
p6020 : sexo

p6040 : ¿ cuantos años tiene?

p6160 : ¿ sabe leer y escribir?

p6500 : ¿ ingresos del mes pasado?

p6426 : ¿ cuanto tiempo lleva trabajando en esta empresa (dias)?


<!----------------------------------------------------------------------------->
<!---------------------------- agrupar bases de datos ------------------------->
<!----------------------------------------------------------------------------->
# 1. group_by + mutate + summarise
<html><div style='float:left'></div><hr color='#000099' size=3px width=850px></html>

<!------------------------------>
<!--------- summarise ---------->         
<!------------------------------>
## 1.1 Summarise

" summarize == summarise "  son sinónimos, la diferente manera de deletrear proviene de Inglaterra (summarize) y américa (summarise). Esta función permite resumir los datos manteniendo la estructura de los datos en estilo dataframe. 

Estructura  array
```{r, echo=FALSE}
table(ocu_1_2$p6020)
```

Estructura  dataframe
```{r, echo=FALSE}
ocu_1_2 %>% summarise( table(p6020))
```


<!------------------------------>
<!------------------------------>
## 1.1 Summarise

* Ejemplos de summarise
```{r}
# Promedio de ingresos en la población ocupada.
ocu_1_2 %>% summarise( media = mean(p6500, na.rm = TRUE)) 

# Frecuencia de hombres y mujeres.
ocu_1_2 %>%summarise(total = table(p6020)) 
```


<!------------------------------>
<!------------------------------>
## 1.1 Summarise

* Ejemplos de summarise
```{r}
# Mediana de ingresos.
ocu_1_2 %>% summarise( mediana = median(p6500, na.rm = TRUE)) 

# quartiles de los ingresos.
ocu_1_2 %>% summarise(quartiles = quantile(p6500, na.rm = TRUE))
```

<!------------------------------>
<!--------- group_by ----------->         
<!------------------------------>


<!------------------------------>
<!------------------------------>
## Group_by

- Agrupar por un dato
<center>
![](pics/one_var.png)
</center>


<!------------------------------>
<!------------------------------>
## Group_by

- Agrupar por dos dato
<center>
![](pics/two_var.png)
</center>

<!------------------------------>
<!-- group_by() + summarise() -->         
<!------------------------------>
## 1.2 Group_by + summarise

Agrupamos por sexo y realizamos summarise para encontrar los ocupados que leen.
```{r, eval=FALSE}
ocu_1_2 %>% group_by(p6020) %>% summarise(lee = table(p6160)) %>% head()
```
```{r, echo=FALSE, warning=FALSE, message=FALSE}
as.data.frame(ocu_1_2 %>% group_by(p6020) %>% summarise(lee = table(p6160)) %>% head())
```

Los ingresos promedios por sexo
```{r, eval=FALSE}
ocu_1_2 %>% group_by(p6020) %>% summarise(ingresos_promedio = mean(p6500, na.rm = TRUE)) %>% head()
```
```{r, echo=FALSE, warning=FALSE, message=FALSE}
as.data.frame(ocu_1_2 %>% group_by(p6020) %>% summarise(ingresos_promedio = mean(p6500, na.rm = TRUE)) %>% head())
```


<!------------------------------>
<!------------------------------>
## 1.2 Group_by + summarise

Podemos agrupar y resumir por dos o mas variables. Ingresos promedios subdividido por analfabetismo y total de cada población.
```{r, eval=FALSE}
ocu_1_2 %>%  group_by(p6020, p6160) %>%
             summarise(total = table(p6020), 
                       ingresos_promedio = mean(p6500, na.rm = TRUE)))
```
```{r, message=FALSE , warning=FALSE, echo=FALSE}
as.data.frame(ocu_1_2 %>% group_by(p6020, p6160) %>%
                          summarise(total = table(p6020), 
                                    ingresos_promedio = mean(p6500, na.rm = TRUE)))
```


<!-------------------------------------->
<!-- group_by() + mutate() + summarize ->
<!-------------------------------------->
## 1.3 Group_by + mutate + summarise

Ejemplo 1: Group_by y summarise son flexible y puede ser utilizada en conjunto con muchas funciones, por ejemplo mutate. 

```{r , eval=FALSE}
ocu_1_2 %>% 
  group_by(p6020) %>% 
  mutate(p6020 = ifelse(test = p6020 == 1, "hombre", "mujer")) %>% 
  summarise(total = table(p6020))
```

```{r, message=FALSE , warning=FALSE, echo=FALSE}
poblacion_sexo = as.data.frame(ocu_1_2 %>% group_by(p6020) %>% 
                                           mutate(p6020 = ifelse(test = p6020 == 1, "hombre", "mujer")) %>% 
                                           summarise(total = table(p6020)))
poblacion_sexo
```


<!------------------------------>
<!------------------------------>
## 1.3 Group_by + mutate + summarise

Ejemplo 2: Podemos realizar multiple ifelse dentro de mutates y agrupar por ellos.  
```{r , eval=FALSE}
 
ocu_1_2 %>% mutate(rango = ifelse(test = p6040 <= 25, "joven",
                                  ifelse(test = p6040 >= 25 & p6040 <= 50, "adulto",
                                         ifelse(test = p6040 >= 50 & p6040 <= 65, "alta_edad", "retirado"))),
                   p6020 = ifelse(test = p6020 == 1, "hombre", "mujer")) %>% 
            group_by(rango, p6020) %>% 
            summarise(total = table(p6020))
```

```{r, message=FALSE , warning=FALSE, echo=FALSE}
rango_sexo = as.data.frame(ocu_1_2 %>% mutate(rango = ifelse(test = p6040 <= 25, "joven",
                                              ifelse(test = p6040 >= 25 & p6040 <= 50, "adulto",
                                                     ifelse(test = p6040 >= 50 & p6040 <= 65, "alta_edad", "retirado"))),
                               p6020 = ifelse(test = p6020 == 1, "hombre", "mujer")) %>% 
                        group_by(rango, p6020) %>% 
                        summarise(total = table(p6020)))
rango_sexo
```

Tip: realizar el mutate antes que el group_by.


<!------------------------------>
<!------------------------------>
## 1.3 Group_by + mutate + summarise

Ejemplo 3:
```{r , eval=FALSE}
 ocu_1_2 %>%  mutate(mes = ifelse(test = mes == 1, "enero", "febrero"),
                     p6020 = ifelse(test = p6020 == 1, "hombre", "mujer"),
                     p6160 = ifelse(test = p6160 == 1, "lee", "no_lee")) %>% 
              group_by(mes, p6020, p6160) %>% 
              summarise(total = table(p6020),
                        ingreso_promedio =  median(p6500, na.rm = TRUE))


```

```{r, message=FALSE , warning=FALSE, echo=FALSE}
mes_sexo_leen_ingreso_promedios = as.data.frame( ocu_1_2 %>%  mutate(mes = ifelse(test = mes == 1, "enero", "febrero"),
                                                                     p6020 = ifelse(test = p6020 == 1, "hombre", "mujer"),
                                                                     p6160 = ifelse(test = p6160 == 1, "lee", "no_lee")) %>% 
                                                              group_by(mes, p6020, p6160) %>% 
                                                              summarise(total = table(p6020),
                                                                        ingreso_promedio =  median(p6500, na.rm = TRUE)))

mes_sexo_leen_ingreso_promedios %>% head(10)
```




<!----------------------------------------------------------------------------->
<!-------------------- Pivotear o transponer bases de datos ------------------->
<!----------------------------------------------------------------------------->
# 2. Pivotear
<html><div style='float:left'></div><hr color='#000099' size=3px width=850px></html>


<!------------------------------>
<!------------------------------>
## Pivot

- Pivot es un intercambio entre el numero de filas y columnas

<center>
![](pics/tidyr-spread-gather.gif)
</center>

Imagen tomada de: [tidyexplain](https://www.garrickadenbuie.com/project/tidyexplain/#spread-and-gather)  


<!------------------------------>
<!--------- pivot_wider -------->         
<!------------------------------>
## 2.1 pivot_wide

- Pivot de 2 columnas, ejemplo:
```{r, eval=FALSE}
rango_sexo %>% pivot_wider(id_cols= c(rango,p6020), 
                           names_from = p6020, 
                           values_from = total )

```

```{r, echo=FALSE}
 wide_rango_sexo = as.data.frame(rango_sexo %>% pivot_wider(id_cols= c(rango,p6020), names_from = p6020, values_from = total))
wide_rango_sexo
```


<!------------------------------>
<!------------------------------>
## 2.1 pivot_wide

- Pivot de 3 columnas, ejemplo:
```{r, eval=FALSE}
mes_sexo_leen_ingreso_promedios %>%  pivot_wider(id_cols= c(mes, p6020, p6160), 
                                                 names_from = p6160, 
                                                 values_from = c(total,ingreso_promedio))
```

```{r, message=FALSE , warning=FALSE, echo=FALSE}
mes_sexo_leen_ingreso_promedios = as.data.frame(mes_sexo_leen_ingreso_promedios %>%  pivot_wider(id_cols= c(mes, p6020, p6160), 
                                                                                                 names_from = p6160, 
                                                                                                 values_from = c(total,ingreso_promedio)))
mes_sexo_leen_ingreso_promedios
```


<!------------------------------>
<!------- pivot_longer() ------->         
<!------------------------------>
## 2.2 pivot_longe

- Pivot devuelta
```{r, eval=FALSE}
wide_rango_sexo %>% pivot_longer( cols = c(hombre,mujer), names_to = "total")
```

```{r, message=FALSE , warning=FALSE, echo=FALSE}
as.data.frame(wide_rango_sexo %>% pivot_longer( cols = c(hombre,mujer), names_to = "total"))
```

<!------------------------------>
## pivot

<center>
![](pics/equation.gif)
</center>

Pivot puede ser un poco complicado, pero después de un poco de practica se vuelve mas fácil, no nos preocupemos habra mas practicas en la próxima clase. 

<!----------------------------------------------------------------------------->
<!--------------------------------- Hoy vimos --------------------------------->
<!----------------------------------------------------------------------------->
#  Hoy vimos...

☑ Summarise

☑ Group_by

☑ Pivot_wide

☑ Pivot_long


<!------------------>
<!--- HTML style --->
<style type="text/css">
.reveal .progress {background: #CC0000 ; color: #CC0000}
.reveal .controls {color: #CC0000}
.reveal h1.title {font-size: 2.4em;color: #CC0000; font-weight: bolde}
.reveal h1.subtitle {font-size:2.0em ; color:#000000}
.reveal section h1 {font-size:1.8em ; color:#CC0000 ; font-weight:bolder ; vertical-align:middle}
.reveal section h2 {font-size:1.4em ; color:#CC0000 ; font-weight:bolde ; text-align:left}
.reveal section h3 {font-size:1.1em ; color:#00000 ; font-weight:bolde ; text-align:left}
.reveal section h4 {font-size:1.0em ; color:#00000 ; font-weight:bolde ; text-align:left}
.reveal section h5 {font-size:0.9em ; color:#00000 ; font-weight:bolde ; text-align:left}
.reveal section p {font-size:0.7em ; color:#00000 ; text-align:left}
.reveal section a {font-size:0.8em ; color:#000099 ; text-align:left}
.reveal section div {align="center";}
.reveal ul {list-style-type:disc ; font-size:1.0em ; color:#00000 ; display: block;}
.reveal ul ul {list-style-type: square; font-size:0.8em ; display: block;}
.reveal ul ul ul {list-style-type: circle; font-size:0.8em ; display: block;}
.reveal section img {border: none;box-shadow: none;}

</style>
